﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace PositionSelectionBusinessLibrary.DataModels
{
    public class CandidateModel
    {
        public int CandidateId { get; private set; }
        public string CandidateName { get; private set; }
        public Collection<string> Skills { get; private set; }
        public CandidateModel(int candidateId, string candidateName, Collection<string> skills)
        {
            CandidateId = candidateId;
            CandidateName = candidateName;
            Skills = skills;
        }
    }
}
