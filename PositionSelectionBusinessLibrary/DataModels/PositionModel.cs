﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace PositionSelectionBusinessLibrary.DataModels
{
    public class PositionModel
    {
        public int PositionId { get; private set; }
        public string PositionTitle { get; private set; }
        public Collection<string> DesirableSkills { get; private set; }
        public PositionModel(int positionId, string positionTitle, Collection<string> desirableSkills)
        {
            PositionId = positionId;
            PositionTitle = positionTitle;
            DesirableSkills = desirableSkills;
        }
    }
}
