﻿using PositionSelectionBusinessLibrary.DataModels;
using PositionSelectionBusinessLibrary.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace PositionSelectionBusinessLibrary.Repositories
{
    public class PositionSelectionRepository: IPositionSelectionRepository
    {
        //public function, search the best candidate's name by position's name. 
        public string GetBestCandidate(string positionName)
        {
            PositionModel position = GetPositionByName(positionName);
            if (position != null)
            {
                CandidateModel bestCandidate = SearchCandidateSkills(position);
                return bestCandidate?.CandidateName;
            }
            return string.Empty;
        }
        //Get the positionModel, searching by positionName
        private PositionModel GetPositionByName(string positionName)
        {
            Collection<PositionModel> positions = GetAllPositions();

            return positions.FirstOrDefault(p => p.PositionTitle == positionName);
        }
        //Use intersect to check the matching skills and get the candidate with the most matching skills
        private CandidateModel SearchCandidateSkills(PositionModel position)
        {
            Collection<CandidateModel> candidates = GetAllCandidates();
            Dictionary<CandidateModel, int> MatchingSkillList = candidates.ToDictionary(c => c,
                c => position.DesirableSkills.Intersect(c.Skills) != null ? position.DesirableSkills.Intersect(c.Skills).Count() : 0);
            return MatchingSkillList.Aggregate((x, y) => x.Value > y.Value ? x : y).Key;
        }
        //Static function to generate the positions colleciton
        private static Collection<PositionModel> GetAllPositions()
        {
            return new Collection<PositionModel>()
            {
                new PositionModel(1,".Net Developer", new Collection<string>()
                    {
                        "C#",
                        "SQL Server",
                        "JavaScript",
                        "DI"
                    }),
                new PositionModel(2,"FrontEnd Developer", new Collection<string>()
                    {
                        "JavaScript",
                        "TypeScript",
                        "React",
                        "Angular"
                    }),
                new PositionModel(3,"Technical Manager", new Collection<string>()
                    {
                        "C#",
                        "SQL Server",
                        "JavaScript",
                        "React",
                        "CI/CD",
                        "PowerShell"
                    })
            };
        }
        //Static function to generate the candidate collection.
        private static Collection<CandidateModel> GetAllCandidates()
        {
            return new Collection<CandidateModel>()
            {
                new CandidateModel(1,"Jack .Net Developer", new Collection<string>()
                    {
                        "C#",
                        "SQL Server",
                        "JavaScript",
                        "Angular"
                    }),
                new CandidateModel(2,"Sam FrontEnd Developer", new Collection<string>()
                    {
                        "JavaScript",
                        "TypeScript",
                        "React",
                        "CI/DI"
                    }),
                new CandidateModel(3,"Kim Technical Manager", new Collection<string>()
                    {
                        "C#",
                        "SQL Server",
                        "Angular",
                        "React",
                        "PowerShell"
                    })
            };
        }
    }
}
