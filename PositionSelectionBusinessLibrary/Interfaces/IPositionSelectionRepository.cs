﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PositionSelectionBusinessLibrary.Interfaces
{
    public interface IPositionSelectionRepository
    {
        string GetBestCandidate(string positionName);
    }
}
