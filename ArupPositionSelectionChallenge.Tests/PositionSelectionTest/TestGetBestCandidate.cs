﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PositionSelectionBusinessLibrary.Repositories;

namespace ArupPositionSelectionChallenge.Tests.PositionSelectionTest
{
    [TestClass]
    public class TestGetBestCandidate
    {
        [TestMethod]
        public void GetBestCandidateForNetDeveloperSucess()
        {
            //Arrange
            string position = ".Net Developer";
            PositionSelectionRepository repo = new PositionSelectionRepository();

            //Act
            string bestCandidate = repo.GetBestCandidate(position);

            //Assert
            Assert.AreEqual("Jack .Net Developer", bestCandidate);
        }
        [TestMethod]
        public void GetBestCandidateForFrontEndDeveloperSucess()
        {
            //Arrange
            string position = "FrontEnd Developer";
            PositionSelectionRepository repo = new PositionSelectionRepository();

            //Act
            string bestCandidate = repo.GetBestCandidate(position);

            //Assert
            Assert.AreEqual("Sam FrontEnd Developer", bestCandidate);
        }
        [TestMethod]
        public void GetBestCandidateForTechnicalManagerSucess()
        {
            //Arrange
            string position = "Technical Manager";
            PositionSelectionRepository repo = new PositionSelectionRepository();

            //Act
            string bestCandidate = repo.GetBestCandidate(position);

            //Assert
            Assert.AreEqual("Kim Technical Manager", bestCandidate);
        }
        [TestMethod]
        public void GetBestCandidateForPositionNotExistsFail()
        {
            //Arrange
            string position = "General Manager";
            PositionSelectionRepository repo = new PositionSelectionRepository();

            //Act
            string bestCandidate = repo.GetBestCandidate(position);

            //Assert
            Assert.AreEqual(string.Empty, bestCandidate);
        }
        [TestMethod]
        public void GetBestCandidateForEmptyPositionFail()
        {
            //Arrange
            string position = string.Empty;
            PositionSelectionRepository repo = new PositionSelectionRepository();

            //Act
            string bestCandidate = repo.GetBestCandidate(position);

            //Assert
            Assert.AreEqual(string.Empty, bestCandidate);
        }
    }
    
}
