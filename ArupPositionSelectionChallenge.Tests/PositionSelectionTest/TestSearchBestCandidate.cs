﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PositionSelectionBusinessLibrary.Interfaces;
using ArupPositionSelectionChallenge.Controllers;
using System.Web.Http;
using System.Web.Http.Results;
using Moq;

namespace ArupPositionSelectionChallenge.Tests.PositionSelectionTest
{
    [TestClass]
    public class TestSearchBestCandidate
    {
        [TestMethod]
        public void SearchBestCandidateNetDeveloperSuccess()
        {
            //Arrange
            string position = ".Net Developer";
            var mockPositionrepo = new Mock<IPositionSelectionRepository>();
            mockPositionrepo.Setup(x => x.GetBestCandidate(position)).Returns("Raphael");
            var controller = new PositionSelectionController(mockPositionrepo.Object);

            //Act
            IHttpActionResult response = controller.SearchBestCandidate(position);
            var contentResult = response as OkNegotiatedContentResult<string>;

            //Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual("Raphael", contentResult.Content);
        }
        [TestMethod]
        public void SearchBestCandidateNonExistPositionFail()
        {
            //Arrange
            string position = "General Manager";
            var mockPositionrepo = new Mock<IPositionSelectionRepository>();
            mockPositionrepo.Setup(x => x.GetBestCandidate(position)).Returns(string.Empty);
            var controller = new PositionSelectionController(mockPositionrepo.Object);

            //Act
            IHttpActionResult response = controller.SearchBestCandidate(position);
            var contentResult = response as NotFoundResult;

            //Assert
            Assert.IsNotNull(contentResult);
        }
    }
}
