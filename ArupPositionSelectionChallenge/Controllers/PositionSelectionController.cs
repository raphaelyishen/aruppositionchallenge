﻿using PositionSelectionBusinessLibrary.Interfaces;
using System.Web.Http;

namespace ArupPositionSelectionChallenge.Controllers
{
    public class PositionSelectionController : ApiController
    {
        private readonly IPositionSelectionRepository _positionSelectionRepo;
        //DI for data repository
        public PositionSelectionController(IPositionSelectionRepository positionSelectionRepo)
        {
            _positionSelectionRepo = positionSelectionRepo;
        }
        /// <summary>
        /// Main API function, calling the repository function to search best candidate for provided positionName.
        /// </summary>
        /// <param name="positionName">position name</param>
        /// <returns>Httpstatus and candidate name(if applicable)</returns>
        [HttpGet]
        public IHttpActionResult SearchBestCandidate(string positionName)
        {
            string bestCandidateName = _positionSelectionRepo.GetBestCandidate(positionName);
            if (string.IsNullOrEmpty(bestCandidateName))
            {
                return NotFound();
            }
            else
            {
                return Ok(bestCandidateName);
            }
        }
    }
}
